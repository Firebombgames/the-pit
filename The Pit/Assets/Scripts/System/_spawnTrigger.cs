﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _spawnTrigger : MonoBehaviour
{
    public Transform[] enemySpawnPoints;
    public GameObject[] enemy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnEnemies()
    {
        for (int i = 0; i < enemySpawnPoints.Length; i++)
        {
            Instantiate(enemy[i], enemySpawnPoints[i].transform.position, enemySpawnPoints[i].transform.rotation);
        }
        Destroy(gameObject);
    }
}
