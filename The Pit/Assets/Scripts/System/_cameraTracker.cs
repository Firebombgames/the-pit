﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _cameraTracker : MonoBehaviour
{
    public GameObject track;
    public float safeZone = 1f;
    public float trackSpeed = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, track.transform.position) > safeZone)
        {
            transform.position=(Vector3.Lerp(transform.position,track.transform.position,Time.deltaTime));
        }
        transform.position=new Vector3(transform.position.x,transform.position.y,-12);
    }
}
