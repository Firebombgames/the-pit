﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class _fpsTracker : MonoBehaviour
{
    public int fps;
    public int frames;
    public float time;
    public Text fpsText;
    
    // Start is called before the first frame update
    void Start()
    {
        fps = 0;
        frames = 0;
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 1)
        {
            time -= 1;
            fps = frames;
            frames = 0;
            fpsText.text = "FPS: " + fps;
        }
        else
        {
            time += Time.deltaTime;
            frames++;
        }
    }
}
