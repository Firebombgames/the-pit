﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Player_Controller : MonoBehaviour
{
    public float moveSpeed = 1f;
    public Rigidbody2D rb;
    public ParticleSystem part;
    public float AttackRadius = 1f;
    public float attackTimer = 0f;
    public float attackTimerMax = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        part = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        //movement
        rb.MovePosition(rb.position + new Vector2((moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime),
                            (moveSpeed * Input.GetAxis("Vertical")) * Time.deltaTime));
        //update the attack radius
        var sh = part.shape;
        sh.radius = AttackRadius;
        GetComponent<CircleCollider2D>().radius = AttackRadius;
        
        //adjust the attack timer
        if (attackTimer != 0)
        {
            attackTimer -= Time.deltaTime;
            if (attackTimer < 0)
            {
                attackTimer = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        other.SendMessage("spawnEnemies");
    }
}
