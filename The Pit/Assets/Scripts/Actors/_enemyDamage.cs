﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class _enemyDamage : MonoBehaviour
{
    public GameObject damageText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "player" && other.GetComponent<_Player_Controller>().attackTimer == 0)
        {
            GameObject d = Instantiate(damageText, transform.position, transform.rotation);
            d.GetComponent<TextMeshPro>().text = "20";
            other.GetComponent<_Player_Controller>().attackTimer =
                other.GetComponent<_Player_Controller>().attackTimerMax;
        }
    }

    
}
