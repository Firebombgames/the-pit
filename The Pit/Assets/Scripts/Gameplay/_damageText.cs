﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class _damageText : MonoBehaviour
{
    public TextMeshPro tm;
    public int state = 1;
    public float timer = 0;
    public float fadeTime = .25f;
    public float moveTime = 1f;
    public float moveSpeed = 1f;
    public bool playerHit = false;
    // Start is called before the first frame update
    void Start()
    {
        tm = this.GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case 1:
                if (playerHit)
                {
                    tm.color=new Color(
                        1f,.25f,.25f,Mathf.Lerp(0,1,timer/fadeTime));
                }
                else
                {
                    tm.color=new Color(
                        .25f,1f,.25f,Mathf.Lerp(0,1,timer/fadeTime));
                }

                timer += Time.deltaTime;
                if (timer >= fadeTime)
                {
                    timer = 0;
                    state++;
                }
                break;
            case 2:
                transform.Translate(0, moveSpeed * Time.deltaTime, 0);
                timer += Time.deltaTime;
                if (timer >= moveTime)
                {
                    timer = 0;
                    state++;
                }
                break;
            default:
                Destroy(gameObject);
                break;
        }
    }
}
